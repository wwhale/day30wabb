(function () {  

angular
.module("PPPApp")
.config(uirouterAppConfig);
uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];


function uirouterAppConfig($stateProvider, $urlRouterProvider){

$stateProvider
    .state("load",{ 
        url : '/load',
        views: {
            'menu': {
            templateUrl: 'app/menu/menulogin.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'load.html',
            controller: 'PPPController as ctrl',
            }
        }

    })
    .state("register", {
        url: "/register",
        views: {
            'menu': {
              templateUrl: 'app/menu/menulogin.html',
              controller: 'PPPController as ctrl',
            },
            'content': {
              templateUrl: 'register.html',
              controller: 'PPPController as ctrl',
            }
        }
    })
    .state("registerotp", {
        url: "/registerotp",
        views: {
            'menu': {
              templateUrl: 'app/menu/menulogin.html',
              controller: 'PPPController as ctrl',
            },
            'content': {
              templateUrl: 'registerotp.html',
              controller: 'PPPController as ctrl',
            }
        }
    })
    .state("unlockform", {
        url: "/unlockform",
        views: {
            'menu': {
              templateUrl: 'app/menu/menulogin.html',
              controller: 'PPPController as ctrl',
            },
            'content': {
              templateUrl: 'unlockform.html',
              controller: 'PPPController as ctrl',
            }
        }
    })

    .state("profile", {
        url: "/profile",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuhome.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'profile.html',
            controller: 'PPPController as ctrl',
            }
        }
    })   

     
    .state("setting", {
        url: "/setting",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuhome.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'setting.html',
            controller: 'PPPController as ctrl',
            }
        }
    }) 

    .state("config", {
        url: "/config",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuhome.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'config.html',
            controller: 'PPPController as ctrl',
            }
        }
    }) 
    .state("add", {
        url: "/add",
        views: {
            'menu': {
            templateUrl: 'app/menu/menulogin.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'addprofile.html',
            controller: 'PPPController as ctrl',
            }
        }
    }) 
    .state("registration", {
        url: "/registration",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuextent.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'registration.html',
            controller: 'PPPController as ctrl',
            }
        }
    }) 
    .state("registrationadd", {
        url: "/registrationadd",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuextent.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'registrationadd.html',
            controller: 'PPPController as ctrl',
            }
        }
    })
    .state("registrationedit", {
        url: "/registrationedit",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuextent.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'registrationedit.html',
            controller: 'PPPController as ctrl',
            }
        }
    }) 
    .state("payform", {
        url: "/payform",
        views: {
            'menu': {
            templateUrl: 'app/menu/menuextent.html',
            controller: 'PPPController as ctrl',
            },
            'content': {
            templateUrl: 'payform.html',
            controller: 'PPPController',
            controllerAs : 'ctrl',
            }
        }
        
    }) 


    $urlRouterProvider.otherwise("/load"); //if can't find any route, it goes to profile 

}

})();

/*

.state('home', {
          url: '/home',
          views: {
            'menu': {
              templateUrl: 'app/menu/menu.html',
              controller: 'MenuCtrl as ctrl',
            },
            'content': {
              templateUrl: 'app/home/home.html',
              controller: 'HomeCtrl as ctrl',
            }
          },
          resolve: {
            user: function(PassportSvc) {
              return PassportSvc.userAuth()
                .then(function(result) {
                  return result.data.user;
                })
                .catch(function(err) {
                  return '';
                });
            }
          },
        }) 
*/