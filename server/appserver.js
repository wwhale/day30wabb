var express = require("express");
var bodyParser = require('body-parser');
var Sequelize = require ("sequelize");
var request = require('request');
var moment = require('moment');

var querystring = require('querystring');
var qs = require('qs');
var https = require('https');
var curl = require('curl');

//var btoa = require('btoa');



var domain = 'sandbox5ef6e4a6870b4cfcab1ee92c0cc89d6f.mailgun.org';
var api_key = 'key-b804b2a4ec3a8ba0ea2d0f2f7902d2f6';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

var node_port = process.env.PORT || 3000;
const PASSANGER_API = "/api/passanger";

const SMSURL = "http://192.168.1.171:8080/v1/sms/";

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="1111"
var connection = new Sequelize(
    'project1',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3308,
        logging: console.log,
        dialect: 'mysql',
        timezone: "+8:00",
        dialectOptions: {
            dateStrings: true,
            typeCast: true 
        },  
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);


var Class = require('./models/class')(connection, Sequelize);
var Contact = require('./models/contact')(connection, Sequelize);

var Parent = require('./models/parent')(connection, Sequelize);
var Passanger = require('./models/passanger')(connection, Sequelize);
var Registration = require('./models/registration')(connection, Sequelize);
var Otp = require('./models/otp')(connection, Sequelize);
var Payment = require('./models/payment')(connection, Sequelize);
var Userlogin = require('./models/userlogin')(connection, Sequelize);


Passanger.hasOne(Registration, {foreignKey: 'regpgrID'})
Passanger.hasMany(Parent, {foreignKey: 'pgrID'})
Parent.hasMany(Contact, {foreignKey: 'conID'})
//Org.hasOne(Passanger, {foreignKey: 'orgID'})
//Class.hasOne(Passanger, {foreignKey: 'claID'})
Registration.hasMany(Payment,{foreignKey: 'payregID'})
Passanger.hasOne(Userlogin, {foreignKey: 'loginpgrID'})


Contact.belongsTo(Parent, {foreignKey: 'parID'})
Parent.belongsTo(Passanger, {foreignKey: 'pgrID'})
//Passanger.belongsTo(Class,{foreignKey: 'claID'})
//Passanger.belongsTo(Registration, {foreignKey: 'pgrID'})
Registration.belongsTo(Passanger, {foreignKey: 'regpgrID'});
Registration.belongsTo(Class,{foreignKey: 'schclassID'});
Payment.belongsTo(Registration,{foreignKey: 'payregID'});
Userlogin.belongsTo(Passanger,{foreignKey: 'loginpgrID'});

/* code for sending emails

var data = {
    from: 'Administrator <IT@JK59.com>',
    to: 'wwhale@hotmail.com',
    subject: 'Validation code for accounts',
    text: 'You validation code is 111111'
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
  });
 */


var dbsToken = '';
var dbsPartyId = '';



var app = express();
/* app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'})); */

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));





app.post("/redirecttoken", (req, res)=>{
    console.log ("====== /redirecttoken =======");
    console.log("hi");
    console.log("req.body");
    console.log("req.body.accessToken");
    var dbsToken = req.body.accessToken;
    console.log(dbsToken);
});

app.get("/redirectdbs"), (req, res)=>{
    console.log ("====== /redirect dbs =======");

    //res.redirect('https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id=ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e&redirect_uri=http%3A%2F%2F118.189.176.177%2Fredirect&scope=Read&response_type=code&state=0399');

    res.redirect('https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id=ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e&redirect_uri=http%3A%2F%2F95b30da5.ngrok.io%2Fredirect&scope=Read&response_type=code&state=0399');
}
/*  try out curl
app.get("/redirect222", (req, res)=>{
    
        console.log(req.query.code);
    
        clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
        clientSecret = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';
    
        //urlTest= 'http://118.189.176.177/#!/payform';
        urlTest= 'http://f79dd2fe.ngrok.io/#!/payform';
    
        var post_data = querystring.stringify({
            code: req.query.code,
            //redirect_uri: 'http://118.189.176.177/redirect',
            redirect_uri: 'http://f79dd2fe.ngrok.io/redirect',
            grant_type: 'token'
        });
    
        var buffer = new Buffer(clientId+':'+clientSecret);
        var apiKey = buffer.toString('base64');
    
            request({
                method: 'POST',
                url: 'https://www.dbs.com/sandbox/api/sg/v1/oauth/tokens',
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded',
                    'Content-Length' : post_data.length,
                    'Authorization': 'Basic '+apiKey
    
                },
                body: post_data
            },
            function(error, response, body) {
                //console.log(error,response,body)
                console.log("response > " + response);
                console.log("response response > " + JSON.stringify(response));
                console.log("response body  parse> " + JSON.parse(body));
                body = JSON.parse(body);
                
                console.log("response.statusCode > " + response.statusCode);
    
                accessToken = body.access_token;
                partyId = body.party_id;
                console.log ("accessToken > " + accessToken);
                console.log ("partyId > " + partyId);
     
                dbsToken = body.access_token;
                dbsPartyId = body.party_id;
    
                console.log("dbsToken > " + dbsToken);
                console.log("dbsPartyId > " + dbsPartyId);
    
                //res.redirect(urlTest);
    
    
                var body = JSON.stringify({
                    "fundTransferDetl": {
                      "partyId": "11845277752388953651",
                      "debitAccountId": "21841900319944140151001",
                      "payeeReference": {
                        "referenceType": "MSISDN",
                        "referenceDesc": "Mobile no.",
                        "reference": "65998899758"
                      },
                      "amount": 5,
                      "transferCurrency": "SGD",
                      "comments": "Transfer",
                      "purpose": "Transfer",
                      "referenceId": "4P3EDAB1C853A004117A330"
                    }
                });
    
                console.log(" post_transacdata > ", body);

                url= 'https://www.dbs.com/sandbox/api/sg/v1/transfers/payNow';

                options = {
                    'Content-Type' : 'application/json',
                    'Content-Length' : Buffer.bytelength(body,'utf8'),
                    'clientId' : clientId,
                    'accessToken': accessToken
                    //'uuid': partyId,
                }

                //curl.post(url, body, options, function(err, response, body) {

                    console.log("response > " + response);
                    console.log("response stringify > " + JSON.stringify(response));
                    console.log("error > " + error);
                    console.log("body > " + body);
                                   
                    //console.log("response.statusCode > " + response.statusCode);
            
                    res.redirect(urlTest);
            
            
                
                    if (!error && response.statusCode == 200) {
                        console.log(" request !error && response.statusCode == 200");
                        //return (response);
                        
                    } else {
                        console.log("----- request ------")
                        //return (response);
                    }

                })

            })
            
    
        });

 */
app.get("/redirect", (req, res)=>{

    console.log(req.query.code);

    clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
    clientSecret = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';

    //urlTest= 'http://118.189.176.177/#!/payform';
    urlTest= 'http://f79dd2fe.ngrok.io/#!/payform';

    var post_data = querystring.stringify({
        code: req.query.code,
        //redirect_uri: 'http://118.189.176.177/redirect',
        redirect_uri: 'http://f79dd2fe.ngrok.io/redirect',
        grant_type: 'token'
    });

    var buffer = new Buffer(clientId+':'+clientSecret);
    var apiKey = buffer.toString('base64');

        request({
            method: 'POST',
            url: 'https://www.dbs.com/sandbox/api/sg/v1/oauth/tokens',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded',
                'Content-Length' : post_data.length,
                'Authorization': 'Basic '+apiKey

            },
            body: post_data
        },
        function(error, response, body) {
            //console.log(error,response,body)
            console.log("response > " + response);
            console.log("response response > " + JSON.stringify(response));
            console.log("response body  parse> " + JSON.parse(body));
            body = JSON.parse(body);
            
            console.log("response.statusCode > " + response.statusCode);

            accessToken = body.access_token;
            partyId = body.party_id;
            console.log ("accessToken > " + accessToken);
            console.log ("partyId > " + partyId);
 
            dbsToken = body.access_token;
            dbsPartyId = body.party_id;

            console.log("dbsToken > " + dbsToken);
            console.log("dbsPartyId > " + dbsPartyId);

            //res.redirect(urlTest);


            var post_transacdata = JSON.stringify({
                "fundTransferDetl": {
                  "partyId": "11845277752388953651",
                  "debitAccountId": "21841900319944140151001",
                  "payeeReference": {
                    "referenceType": "MSISDN",
                    "referenceDesc": "Mobile no.",
                    "reference": "65998899758"
                  },
                  "amount": 5,
                  "transferCurrency": "SGD",
                  "comments": "Transfer",
                  "purpose": "Transfer",
                  "referenceId": "4P3EDAB1C853A004117A330"
                }
              });

              console.log(" post_transacdata > ", post_transacdata);

              request({
                method: 'POST',
                url: 'https://www.dbs.com/sandbox/api/sg/v1/transfers/payNow',
                headers: {
                    'Content-Type' : 'application/json',
                    'Content-Length' : Buffer.byteLength(post_transacdata,'utf8'),
                    'clientId' : clientId,
                    'accessToken': accessToken
                    //'uuid': partyId,
                },
                body: post_transacdata
            },
            function(error, response, body) {
                //console.log(error,response,body)
                console.log("response > " + response);
                //console.log("response stringify > " + JSON.stringify(response));
                console.log("error > " + error);
                console.log("body > " + body);
            /*     console.log("response body  parse> " + JSON.parse(body));
                body = JSON.parse(body); */
                
                console.log("response.statusCode > " + response.statusCode);
        
                      res.redirect(urlTest);
        
        
        
            if (!error && response.statusCode == 200) {
                console.log(" request !error && response.statusCode == 200");
                //return (response);
                
            } else {
                console.log("----- request ------")
                //return (response);
            }
            })

        if (!error && response.statusCode == 200) {
            console.log(" request !error && response.statusCode == 200");
            //return (response);
            
        } else {
            console.log("----- request ------")
            //return (response);
        }
        })

    });
    
app.post("/dbspay", (req, res)=>{

    console.log("=====/dbspay====");
    console.log("dbsToken > " + dbsToken);
    console.log("dbsPartyId > " + dbsPartyId);

    
    clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
    clientSecret = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';

    accessToken = dbsToken;

    var post_transacdata = JSON.stringify({
        "fundTransferDetl": {
          "partyId": "11845277752388953651",
          "debitAccountId": "21841900319944140151001",
          "payeeReference": {
            "referenceType": "MSISDN",
            "referenceDesc": "Mobile no.",
            "reference": "65998899758"
          },
          "amount": 5,
          "transferCurrency": "string",
          "comments": "Transfer",
          "purpose": "Transfer",
          "referenceId": "4P3EDAB1C853A004117A330"
        }
      });

 
/* 
    var post_transacdata = JSON.stringify({
        fundTransferDetl: {
            partyId: '11845277752388953651',
            debitAccountId: '21841900319944140151001',
            payeeReference:{
                referenceType: 'MSISDN',
                referenceDesc: 'Mobile no.',
                reference: '65998899758'
            },
            amount: 5,
            transferCurrency: 'SGD',
            comments: 'Transfer',
            purpose: 'Transfer',
            referenceId: '4P3EDAB1C853A004117A330'
        }
    });

    */
   
    request({
        method: 'POST',
        url: 'https://www.dbs.com/sandbox/api/sg/v1/transfers/payNow',
        headers: {
            'Content-Type' : 'application/json',
            'Content-Length' : post_transacdata.length,
            'clientId' : clientId,
            'accessToken': accessToken,
            'uuid': partyId,
        },
        body: post_transacdata
    },
    function(error, response, body) {
        //console.log(error,response,body)
        console.log("response > " + response);
        console.log("response stringify > " + JSON.stringify(response));
        console.log("body > " + body);
    /*     console.log("response body  parse> " + JSON.parse(body));
        body = JSON.parse(body); */
        
        console.log("response.statusCode > " + response.statusCode);

              res.redirect('http://118.189.176.177/#!/load');



    if (!error && response.statusCode == 200) {
        console.log(" request !error && response.statusCode == 200");
        //return (response);
        
    } else {
        console.log("----- request ------")
        //return (response);
    }
    })

});

app.get("/redirect222", (req, res)=>{
    
        console.log ("====== /redirect =======");

        apiHost = 'www.dbs.com';
        apiUrl = '/sandbox/api/sg/v1';
        
        clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
        clientSecret = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';
 
        partyId = {}
        accessToken = {};

        redirectUrl = 'http://118.189.176.177/redirect' ;
        //redirectUrl = 'http://208daf7b.ngrok.io/redirect';

        console.log (req.query.code);
        urlTest= 'http://118.189.176.177/#!/payform';
        
        var post_data = querystring.stringify({
            code: req.query.code,
            redirect_uri: redirectUrl,
            grant_type: 'token'
        });

        console.log("post_data > " + post_data);
        var buffer = new Buffer(clientId+':'+clientSecret);
        var apiKey = buffer.toString('base64');

        var post_options = {
            host: apiHost,
            port: '443',
            path: apiUrl + '/oauth/tokens',
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded',
                'Content-Length' : post_data.length,
                'Authorization': 'Basic '+apiKey

            }
        };
    
    
        https.request(post_options, results => {
            var data = '';

            results.on('data', function(chunk) {
                data += chunk;

            }).on('end', function() {
                var result = JSON.parse(data);
                console.log("result data > " + JSON.stringify(result));
                
                //res.status(200).json(result);
               
                
                //console.log(JSON.stringify(result));
                //console.log("result 2 > " + result.access_token);
                accessToken = result.access_token;
                partyId = result.party_id;
                console.log ("accessToken > " + accessToken);
                console.log ("partyId > " + partyId);
     
                dbsToken = result.access_token;
                dbsPartyId = result.party_id;

                console.log("dbsToken > " + dbsToken);
                console.log("dbsPartyId > " + dbsPartyId);

                res.redirect(urlTest);

                });

                results.on('error', err => {
                        res.status(results.statusCode).json(JSON.parse(err.toString()));
                    });
            }).write(post_data);
            
   
        });
    
  
        


//====================


app.post("/dbspay22222", (req, res)=>{

           
    /* 
                var post_transacdata = JSON.stringify({
                    fundTransferDetl: {
                        //partyId: 11845277752388953651,
                        partyId: 11845277752388953651,
                        debitAccountId: 21841900319944140151001,
                        payeeReference:{
                            referenceType: 'MSISDN',
                            referenceDesc: 'Mobile no.',
                            reference: '65998899758'
                        },
                        amount: 5,
                        transferCurrency: 'SGD',
                        comments: 'Transfer',
                        purpose: 'Transfer',
                        referenceId: '4P3EDAB1C853A004117A330'
                    }
               
                });

 */
            console.log("=====/dbspay====");
            console.log("dbsToken > " + dbsToken);
            console.log("dbsPartyId > " + dbsPartyId);

            apiHost = 'www.dbs.com';
            apiUrl = '/sandbox/api/sg/v1';
            
            clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
            clientSecret = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';

            accessToken = dbsToken;

            var post_transacdata = JSON.stringify({
                fundTransferDetl: {
                    partyId: dbsPartyId,
                    debitAccountId: '21841900319944140151001',
                    payeeReference:{
                        referenceType: 'MSISDN',
                        referenceDesc: 'Mobile no.',
                        reference: '65998899758'
                    },
                    amount: 5,
                    transferCurrency: 'SGD',
                    comments: 'Transfer',
                    purpose: 'Transfer',
                    referenceId: '4P3EDAB1C853A004117A330'
                }
            });

            
            var post_transacoptions = {
            host: apiHost,
            port: '443',
            path: apiUrl + '/transfers/payNow',
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',    
                'Content-Length' : post_transacdata.length,
                'clientId' : clientId,
                'accessToken': accessToken,
              }
            };


            console.log("post_transacdata > ", post_transacdata);

            https.request(post_transacoptions, results => {

            var data = '';

            results.on('data', function(chunk) {
                
                data += chunk;
            }).on('end', function() {
                
                //result = JSON.stringify(data)
                //result = res.body;
   
                console.log("result > " + data); 

                //console.log("res.body > " + JSON.stringify(res.body));
                //console.log("result status > " + results.statusCode);
                /*  var result = JSON.parse(data);
                console.log("result > " + result); */
                res.status(200).json(data); 

                console.log(" send transaction complete");



            });     
            results.on('error', err => {
                console.log(">>>> error = ", err);
                res.status(results.statusCode).json(JSON.parse(err.toString()));
            });
            }).write(post_transacdata);



});


/* 
app.get("/redirect", (req, res)=>{

    console.log ("====== /redirect =======");



    var clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
    var secretKey = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';
   // var apiKey = 'Y2xpZW50SWQyOnRlc3Q=';
    var apiKey  = 'ZmY4YjViMmEtOWI5Zi00ZWUxLWJlOWMtMjNiMWU1MGM5NjZlOjQ2NGFjN2FiLTlhMGItNDVkZC04MWVkLWJmNzRkODdkZDJmMA==';
    
    var dbsAuthCode = req.query.code;
    console.log(dbsAuthCode);
    
    dbsAuthCode = encodeURIComponent(dbsAuthCode);

    var url = "https://www.dbs.com/sandbox/api/sg/v1/oauth/tokens -H 'Authorization:Basic ";
    console.log ("url1 > " + url);

    //var auth = "NjQ0NjVjZTQtNjk2MS00MTU2LWIyOTItNjQzYmNmMWJmOGIwOjQ0YjVhMWJjLTVlYzMtNDM5Zi1iN2EwLWY2YWUxNTkzZGE4MA==' -d 'code=";

    var auth = new Buffer(secretKey + ":" + clientId).toString("base64")
    console.log (auth);

    var uri = "http://b9d1b437.ngrok.io/redirect";
    //var uri = encodeURI(redirecturi);
     uri = encodeURIComponent(uri);
    
    //url = url + auth + "' -d 'code=" + dbsAuthCode + "&redirect_uri=" + uri +"&grant_type=token'";
    url = url + apiKey + "' -d 'code=" + dbsAuthCode + "&redirect_uri=" + uri +"&grant_type=token'";
    console.log ("final url  > " + url);  
    
   
     request(url, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        console.log(res);
        console.log("response > " + res.statusCode);
        if (res.statusCode ==200){
            console.log("200 res.accessToken > " + res.accessToken);
            console.log("200 res.body.accessToken > " + res.body.accessToken);
            var dbsToken = res.body.accessToken;
            console.log(dbsToken);

        }
        console.log(body.accessToken);
      });
   */
  
//=====================
 /* 
  var dbsAuthCode = req.query.code;
  console.log(dbsAuthCode);
    var clientId = 'ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e';
    var secretKey = '464ac7ab-9a0b-45dd-81ed-bf74d87dd2f0';
    //var apiKey = 'Y2xpZW50SWQyOnRlc3Q=';
     var apiKey  = 'ZmY4YjViMmEtOWI5Zi00ZWUxLWJlOWMtMjNiMWU1MGM5NjZlOjQ2NGFjN2FiLTlhMGItNDVkZC04MWVkLWJmNzRkODdkZDJmMA==';
    var post_data = JSON.stringify({
   
            code: req.query.code,
            redirect_uri: 'http://b9d1b437.ngrok.io/redirect',
            grant_type: 'token'
        });
    
        var post_options = {
            host: 'www.dbs.com',
            port: '443',
            path: '/sandbox/api/sg/v1/oauth/tokens',
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
                'Content-Length' : Buffer.byteLength(post_data, 'utf8'),
                'Authorization': 'Basic '+ apiKey
                //'Authorization': 'Basic '+ dbsAuthCode
                //'Authorization': 'Basic ' + btoa(clientId + ":" + secretKey) 
                //'Authorization': 'Basic ' + new Buffer(secretKey + ":" + clientId).toString("base64")
            }
        };

        console.log(">> post = " + JSON.stringify(post_options));
    
        https.request(post_options, results => {
            var data = [];
            
            
            results.on('data', function(chunk) {
                console.log(">>> data = " + chunk);
                data.push(chunk);
            }).on('end', function() {
                console.log(">>>> data = " + data);
                var buffer = Buffer.concat(data);
                var token = buffer.toString('base64');
                console.log("token > " + token);
                res.status(200).json({token});
            });
    
            results.on('error', err => {
                res.status(results.statusCode).json(JSON.parse(err.toString()));
            });
        }).write(post_data);
    
    });   
    
   */
 
 //-======================
 

// method 3
/* 
app.get("/redirect", (req, res)=>{
    
        console.log ("====== /redirect =======");
    
        var uri = 'http://118.189.176.177/redirect';
        //uri = encodeURIComponent(uri);

        var url = 'https://www.dbs.com/sandbox/api/sg/v1/oauth/tokens';    
       

        var dbsAuthCode = req.query.code;
        //dbsAuthCode = encodeURIComponent(dbsAuthCode);
        console.log(dbsAuthCode);

        var clientid = '64465ce4-6961-4156-b292-643bcf1bf8b0';
        var clientsecret = '44b5a1bc-5ec3-439f-b7a0-f6ae1593da80';
        //var apiKey  = 'ZmY4YjViMmEtOWI5Zi00ZWUxLWJlOWMtMjNiMWU1MGM5NjZlOjQ2NGFjN2FiLTlhMGItNDVkZC04MWVkLWJmNzRkODdkZDJmMA==';
        var apiKey = 'Y2xpZW50SWQyOnRlc3Q=';
        autha = "Basic " + apiKey;



        var urla = url + clientid + ":" + clientsecret + "@www.dbs.com/sandbox/api/sg/v1/oauth/tokens";

        var urlb = "https://www.dbs.com/sandbox/api/sg/v1/oauth/tokens -H 'Authorization:Basic NjQ0NjVjZTQtNjk2MS00MTU2LWIyOTItNjQzYmNmMWJmOGIwOjQ0YjVhMWJjLTVlYzMtNDM5Zi1iN2EwLWY2YWUxNTkzZGE4MA==' -d 'code="; 
 
        var urlc = urlb + dbsAuthCode + "&redirect_uri=" + uri + "&grant_type=token'";
        //console.log(urlc);

        urld = "https://www.dbs.com/sandbox/api/sg/v1/oauth/tokens";
        
        request({
            method: 'POST',
            url:  urld,
            headers: {

                'Authorization': 'Basic '+ apiKey
            },
            qs:{
                code: req.query.code,
                redirect_uri: uri,
                grant_type: 'token'
    
            },  
        },
        function(error, response, body) {
            //console.log(error,response,body)
            console.log(response.data);
            console.log("response.statusCode > " + response.statusCode);

        if (!error && response.statusCode == 200) {
            console.log(" request !error && response.statusCode == 200");
            //return (response);
            
        } else {
            console.log("----- request ------")
            //return (response);
        }
    })
    
 });    
    */ 

 

 

function loginSave(contactno){

   

    Contact.findOne({
        where: {contactNo: contactno}
        ,include:[{
            model: Parent
            ,include:[{
                model:Passanger
                ,include:[{
                    where: {year: 2017}
                    ,model:Registration
                    ,include:{
                        model: Class}

                }]
            }]
        }]
            
        })
    .then((result)=>{

        var pgrID = result.parent.passanger.registration.regpgrID;
        console.log(pgrID);

        Userlogin.findAll({
            where: {contactNo: contactno, currentSess: 'Y'}
                            
            })
            .then( result=>{
    
                console.log("Userlogin findAll> " + JSON.stringify(result));
    
                console.log(result.length);
                
                if (result.length !=0 ){
                    console.log("previous session detected");
                }else{
                    Userlogin.create({
                        loginpgrID: pgrID,
                        contactNo: contactno,
                        loginTime: Date(),
                        currentSess: 'Y',
                        
                        })
                        .then(function (result) {
                            console.log(JSON.stringify(result));
                            //res.status(200).json(result);
                        })
                        .catch(function (err) {
                            console.log(JSON.stringify(err));
                            //res.status(501).json(err);
                        });        
                }
            }); 

   //console.log("server results.. > " + JSON.stringify(result)); 
   /* console.log(result.parent.parID);
   console.log(result.parent.pgrID);
   console.log(result.parent.passanger.pgrID);  */      
   //console.log(result.parent.passanger.pgrName);

    
    })





             

}

function paymentSave(){


            /* Payment.create({
            payregID: 1,
            paypgrID: 3,
            payDate: new Date(),
            payYear: 2017,
            payMth:13,
            amount: 22.20,
            payContact: 10000003,
            payMode: 'CASH',

            })
            .then(function (result) {
                console.log(result)
                //res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                //res.status(501).json(err);
         });        
 */

         Payment.findAll({
            where: {payregID: 1}
            //,attributes: ['payID', 'payregID']   
            ,include: [{
                where:{year: 2017}
                //,attributes: ['regID']              
                ,model: Registration
                }]
        
            })
        
        
            .then(result => {
            console.log(JSON.stringify(result));
        });

}

//paymentSave();

function otpDelete(contactno, callback){

    Otp.findAll({
        where: {contactNo: contactno}
                        
        })
        .then( result=>{

            console.log("otpDelete findAll > " + JSON.stringify(result));

            console.log(result.length);
            
            if (result.length !=0){
            //console.log(result[0].otpID);
                var where = {};
               
                for(i=0; i<result.length; i++) {
                
                where.otpID = result[i].otpID;
                
                Otp.destroy({
                        where: where
                    })
                    .then(function (result) {
                        if (result == "1")
                            console.log(" delete sucessful")
                            //res.json({success: true});
                        else
                            console.log(" delete un-sucessful")
                            //res.json({success: false});
                    })
                    .catch(function (err) {
                        console.log("-- DELETE otpDelete catch(): \n" + JSON.stringify(err));
                    });
                }//end for loop
            } // end if
        }).catch((err)=>{
            console.log(err);
            //callback(500);
            console.log("otpDelete server issue");
        });  // end of Otp.findAll  

        callback(200);
} // end otpDelete




function otpGen (obj, callback){
    var randomOtp = function (len) {
        return Math.floor(Math.pow(10, len-1) + Math.random() * (Math.pow(10, len) - Math.pow(10, len-1) - 1));
    }
    console.log("-------start otpgen------");
    //return randomOtp(6);
    callback( randomOtp(6));

}

function otpSave(contactno,otp){

    /* var contactno = 10000003;
    var otp = 112233; */
    console.log("-------start otpSave------");
    Contact.findOne({
        where: {contactNo: contactno}
        ,include:[{
            model: Parent
            ,include:[{
                model:Passanger
                ,include:[{
                    where: {year: 2017}
                    ,model:Registration
                    ,include:[{
                        model:Class
                        }] 
                    }]
                }]
             }]
            
        })
        .then((result)=>{
        //console.log("server otpSave results.. > " + JSON.stringify(result)); 
        //console.log("server otpSave result.regpgrID for .... > " + result.parent.passanger.registration.regpgrID); 

        Otp.create({
            contactNo: contactno,
            pgrID: result.parent.passanger.registration.regpgrID,
            otp: otp,
            timeOtp: new Date(),

            })
            .then(function (result) {
                console.log("otpSave > " + result)
            })
            .catch(function (err) {
                console.log("otpSave > " + err);
                //res.status(501).json(err);
            });


        /* console.log(result.parent.parID);
        console.log(result.parent.pgrID);
        console.log(result.parent.passanger.pgrID);  */      
        //console.log(result.parent.passanger.pgrName);

        //  res.status(200).json(result);
        }).catch((err)=>{
            console.log("otpSave > " + err);
        //   res.status(500).json(error);
        });  

}


function messageSend(phone,message) {

    request({
            method: 'POST',
            url:  SMSURL,
            qs:{
                phone: phone,
                message: message
            },
        },
        function(error, response, body) {
            //console.log(error,response,body)
            console.log("request > " + response.statusCode)
            //console.log(response);

          if (!error && response.statusCode == 200) {
            console.log(" request !error && response.statusCode == 200");
            //return (response);
            
          } else {
            console.log("----- request error------")
            //return (response);
          }
        }
      );

}

//messageSend("97229759", "hello");

app.post("/api/sms", (req, res) =>{

    console.log("=====send sms =====");
    var usercontactno = req.body.contactno;
    console.log(usercontactno);


    otpDelete(usercontactno,  (status) =>{
    
        console.log("====/api/sms  otpDelete ==== > " + status );
        if (status == 200){
            otpGen(6, function (otp){

                otpSave(usercontactno, otp);
                
                var message = "Your jkpass OTP is : " + otp + ". Expires in 2 minutes. Thank You.";
                console.log("otp details > " + usercontactno + " / " + otp);

                // for testing purpose using my phone
                var contactno = 97229759;
                messageSend(contactno, message);
                
                //For production where contact is from web entry
                //messageSend(usercontactno, message);           
                
                res.status(200).end();

            })
        }else{
            res.status(500).end();
        }
    })  
  
  
});


/*     app.post('/sms', function (req, res) {
    const body = req.body.Body
    res.set('Content-Type', 'text/plain')
    res.send(`You sent: ${body} to Express`)
  }) */


  app.post("/api/chkotp", (req, res)=>{
    console.log("..... /api/chkotp ..." + JSON.stringify(req.body));

    var contactno = req.body.mobilen;
    var otp = req.body.otp;

    Otp.findOne({
        where: {contactNo: contactno}
                        
        })
        .then( result=>{

            console.log("chkotp results> " + JSON.stringify(result)); 
            console.log("chkotp otp > " + result.otp);
            console.log("otp > " + otp);

            console.log("result.timeOtp > " + result.timeOtp);

            var timeOtp = result.timeOtp;
            var timeNow = moment().format();
            
            console.log("result.timeOtp > " + timeOtp);
            console.log("result.moment(timeOtp) > " + moment(timeOtp).unix());
            console.log("result.timeNow > " + timeNow);

            var timeDiff = moment(timeNow).unix() - moment(timeOtp).unix();
            console.log("timeDiff > " + timeDiff);


            if (otp == result.otp && timeDiff <= 120){
                console.log("otp OK");
                otpDelete(contactno,  (status) =>{
                    if (status == 200)
                    console.log("otp delete")
                })
                loginSave(contactno);
                res.status(200).end();
                
            }else if (otp != result.otp) {
                res.status(201).end();
                console.log("otp is not correct")
            } else if (timeDiff >120){
                otpDelete(contactno,  (status) =>{
                    if (status == 200)
                    console.log("otp delete")
                })
                res.status(202).end();
                console.log("otp is more than 2 minutes")   
            }
        }).catch((error)=>{
            console.log(error);
            res.status(500).json(error);
            console.log("chkotp server issue");
        });  // end of Otp.findOne  


});// end of /api/chkotp


app.get("/api/chkcontact"+"/:contactno", (req, res)=>{
    console.log("..... get by contactno ..." + req.params.contactno);
  
    var contactno = req.params.contactno;
    console.log("contactno in app.get > " + contactno);
    //var whereClause = {limit: 1, where: {contactNo: contactno}};
    Contact.findOne({
            where: {contactNo: contactno}
                            
            })
        .then(result=>{

        //console.log("server results> " + JSON.stringify(result)); 
       // console.log("server results > " + result.contactType);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.get("/api/passanger"+"/:contactno", (req, res)=>{
    console.log("..... get by contactno ..." + req.params.contactno);
    
    var contactno = req.params.contactno;
    console.log("contactno in app.get > " + contactno);
    //var whereClause = {limit: 1, where: {contactNo: contactno}};
    Contact.findOne({
            where: {contactNo: contactno}
            ,include:[{
                model: Parent
                ,include:[{
                    model:Passanger
                    ,include:[{
                        where: {year: 2017}
                        ,model:Registration
                        ,include:{
                            model: Class}

                    }]
                }]
            }]
                
            })
        .then((result)=>{
       //console.log("server results.. > " + JSON.stringify(result)); 
       /* console.log(result.parent.parID);
       console.log(result.parent.pgrID);
       console.log(result.parent.passanger.pgrID);  */      
       //console.log(result.parent.passanger.pgrName);

        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});










app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
});

module.exports = app;










/*

//Class.belongsTo(Organization, {foreignKey: 'orgID'});

//Contact.belongsToMany(Passanger, {through: 'Parent'});




/* sql query API

const mysql = require('mysql');
const pool = mysql.createPool({
    host: "localhost", port: 3308,
    user: "root", password: "1111",
    database: "project1",
    connectionLimit: 50

});


        Class.findAll({
                where: {
                    // This where condition filters the findAll result so that it only includes employee names and
                    // employee numbers that have the searchstring as a substring (e.g., if user entered 's' as search
                    // string, the following
                    $or: [
                        {school: {$like: "%" + '?' + "%"}},
                        {class: {$like: "%" + '?' + "%"}},
                        //{year: {$like: "%" + req.query.searchString + "%"}}
                    ]
                }

                , limit: 100
            })
            .then(function (result) {
            console.log(result.status);
            console.log(json(result));
            })
            .catch(function (err) {
            console.log(error);
            }); 



app.get('/api/customers', function(req, resp){
    
         pool.getConnection(function(err, conn){
            if(err){
                resp.status(500).json(err);
                console.log(500);
                return;
            } 
            conn.query("select * from class",
                function(err, result){
                        if(err){
                            resp.status(500).json(err);
                         console.log(500);
                        }
                        else {
                             console.log(200);
                             console.log(result);
                            resp.status(200).json(err);  
                        }
    
                     conn.release();
                    }
                )
        })
    
    });



app.get('/api/customers', function(req, resp){
    
         pool.getConnection(function(err, conn){
            if(err){
                resp.status(500).json(err);
                console.log(500);
                return;
            } 
            conn.query('select * from class where orgID = 2',
            function(err, results){
                    if(err)
                        resp.status(500).json(err);
                    else {
                    if (results.length <=0)
                        resp.status(404)
                            .end("Customer not found: " + 3);  
                    else
                    resp.status(200).json(results);
                }
                 conn.release();
                }
            )
        })
    
    });


app.get('/api/customers/:custId', function(req, resp){
    
        pool.getConnection(function(err, conn){
            if(err){
                resp.status(500).json(err);
                return;
            }
            conn.query('select * from CUSTOMER where customer_id = ?', [req.params.cusId],
                function(err, result){
                        if(err)
                            resp.status(500).json(err);
                        else {
                        if (results.length <=0)
                            resp.status(404)
                                .end("Customer not found: " + req.params.cusId);  
                        else
                        resp.status(200).json(results[0]);
                    }
                     conn.release();
                    }
                )
        })
    
    });

    ====== end sql ===== 

Contact.hasMany(Parent, {foreignKey: 'parID'})
Passanger.hasMany(Parent, {foreignKey: 'pgrID'})
Parent.hasMany(Contact, {foreignKey: 'parID'})
Org.hasOne(Passanger, {foreignKey: 'orgID'})
Class.hasOne(Passanger, {foreignKey: 'claID'});

Contact.belongsToMany(Passanger, {through: 'Parent'})


Contact.belongsTo(Parent, {foreignKey: 'parID'});
Parent.belongsTo(Passanger, {foreignKey: 'pgrID'})

Class.belongsTo(Org, {foreignKey: 'claID'})



var mobilen = req.query.mobilen;
    var serviceprovider = req.query.serviceprovider;
    var mobilenum = parseInt(req.query.mobilen);

Class.findOne().then(Class => {
    console.log(Class.get('class'));
  });

Class.findAll().then(Class => {
    console.log(Class)
  });


connection
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});


// retrieve 
app.get(PASSANGER_API, (req, res)=>{
    console.log("search > " + req.param.mobilen);
    console.log("search > " + JSON.stringify(req.param));
    console.log("searchbody > " + JSON.stringify(req.body));
    console.log("search > " + JSON.stringify(req.query));


    var rbody = req.body;
    console.log("search6 > " + rbody.mobilen);

    Class.findAll().then(results=>{
        console.log("======= find all =======");

        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
    
 
    
    Class.findAll().then((results)=>{
        console.log("======= find all =======")

        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });   

});
*/