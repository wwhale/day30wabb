module.exports = function(connection, Sequelize){
    
        var Organization = connection.define('organization', {
            orgID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            orgName: {
                type: Sequelize.STRING,
                allowNull: false
            },

        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Organization;
    }