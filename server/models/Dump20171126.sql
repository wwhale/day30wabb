-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: project1
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `claID` int(11) NOT NULL AUTO_INCREMENT,
  `orgID` int(11) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `class` varchar(10) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `remark` varchar(256) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `createName` varchar(256) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `udpateDateTime` datetime DEFAULT NULL,
  `updateName` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`claID`),
  UNIQUE KEY `claID_UNIQUE` (`claID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (1,1,'One Primary School','1a',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'One Primary School','1b',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,'One Primary School','1c',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,'One Primary School','2a',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,'One Primary School','2b',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,1,'One Primary School','2c',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,2,'Two Primary School','1a2',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,2,'Two Primary School','1b2',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,2,'Two Primary School','1c2',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,2,'Two Primary School','3a2',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,2,'Two Primary School','3b2',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,2,'Two Primary School','3b2',2017,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `conID` int(11) NOT NULL AUTO_INCREMENT,
  `parID` int(11) DEFAULT NULL,
  `contactNo` varchar(20) DEFAULT NULL,
  `contactType` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `remark` varchar(256) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `createName` varchar(256) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `updateName` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`conID`),
  UNIQUE KEY `conID_UNIQUE` (`conID`),
  KEY `parID_idx` (`parID`),
  CONSTRAINT `parID` FOREIGN KEY (`parID`) REFERENCES `parent` (`parID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,1,'10000001','mobile',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'10000002','mobile',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,3,'10000003','mobile',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,4,'10000004','mobile',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,'10000005','Home',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,2,'10000006','Home',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,3,'10000007','Home',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `orgID` int(11) NOT NULL AUTO_INCREMENT,
  `orgName` varchar(256) DEFAULT NULL,
  `postcode` int(6) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `building` varchar(256) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `active` varchar(2) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `createName` varchar(256) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `updateName` varchar(256) DEFAULT NULL,
  `claID` int(11) DEFAULT NULL,
  PRIMARY KEY (`orgID`),
  UNIQUE KEY `orgID_UNIQUE` (`orgID`),
  KEY `claID_idx` (`claID`),
  CONSTRAINT `claID` FOREIGN KEY (`claID`) REFERENCES `class` (`claID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
INSERT INTO `organization` VALUES (1,'One Primary School',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Two Primary School',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Three Primary School',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parent`
--

DROP TABLE IF EXISTS `parent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent` (
  `parID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(256) DEFAULT NULL,
  `lastName` varchar(256) DEFAULT NULL,
  `parName` varchar(256) DEFAULT NULL,
  `relation` varchar(45) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `remark` varchar(256) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `createName` varchar(256) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `updateName` varchar(256) DEFAULT NULL,
  `pgrID` int(11) DEFAULT NULL,
  PRIMARY KEY (`parID`),
  UNIQUE KEY `parID_UNIQUE` (`parID`),
  KEY `pgrID_idx` (`pgrID`),
  CONSTRAINT `pgrID` FOREIGN KEY (`pgrID`) REFERENCES `passanger` (`pgrID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parent`
--

LOCK TABLES `parent` WRITE;
/*!40000 ALTER TABLE `parent` DISABLE KEYS */;
INSERT INTO `parent` VALUES (1,'P1name','P1last','P1name P1last','Father',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'P2name','P2last','P2name P2last','Father',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(3,'P3name','P3last','P3name P3last','Father',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,3),(4,'P4name','P4last','P4name P4last','Mother',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(5,'P5name','P5last','P5name P5last','Gurdian',NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `parent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passanger`
--

DROP TABLE IF EXISTS `passanger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passanger` (
  `pgrID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(256) DEFAULT NULL,
  `lastName` varchar(256) DEFAULT NULL,
  `pgrName` varchar(256) DEFAULT NULL,
  `blkNo` varchar(8) DEFAULT NULL,
  `unitNo` varchar(8) DEFAULT NULL,
  `buildingName` varchar(256) DEFAULT NULL,
  `streetName` varchar(256) DEFAULT NULL,
  `postCode` int(6) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `createName` varchar(256) DEFAULT NULL,
  `udpateDate` date DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `updateName` varchar(256) DEFAULT NULL,
  `orgID` int(11) DEFAULT NULL,
  `claID` int(11) DEFAULT NULL,
  PRIMARY KEY (`pgrID`),
  UNIQUE KEY `pgrID_UNIQUE` (`pgrID`),
  KEY `orgID_idx` (`orgID`),
  KEY `claID_idx` (`claID`),
  CONSTRAINT `orgID` FOREIGN KEY (`orgID`) REFERENCES `organization` (`orgID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passanger`
--

LOCK TABLES `passanger` WRITE;
/*!40000 ALTER TABLE `passanger` DISABLE KEYS */;
INSERT INTO `passanger` VALUES (1,'Afirstname','Alastname','Alastname Afirstname','100','01-01',NULL,NULL,100001,'100 address Street #01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(2,'Bfirstname','Blastname','Blastname Bfirstname','200','02-02',NULL,NULL,200002,'200 address street #02-02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2),(3,'Cfirstname','Clastname','Clastname Cfirstname','300','03-03',NULL,NULL,300003,'300 address street #03-03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,9),(4,'Dfirstname','Dlastname','Dlastname Dfirstname','400','04-04',NULL,NULL,400004,'400 address street #04-04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,10);
/*!40000 ALTER TABLE `passanger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration` (
  `regID` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `regpgrID` int(11) DEFAULT NULL,
  `schclassID` int(11) DEFAULT NULL,
  `sess` varchar(2) DEFAULT NULL,
  `startTakeDate` date DEFAULT NULL,
  `stopTakeDate` date DEFAULT NULL,
  `startTakeMth` date DEFAULT NULL,
  `stopTakeMth` date DEFAULT NULL,
  `serviceType` varchar(20) DEFAULT NULL,
  `payType` varchar(20) DEFAULT NULL,
  `payScheme` varchar(20) DEFAULT NULL,
  `pickupPl` varchar(255) DEFAULT NULL,
  `pickupPostal` int(6) DEFAULT NULL,
  `pickupAddr` varchar(255) DEFAULT NULL,
  `returnLoc` varchar(255) DEFAULT NULL,
  `returnPostal` int(6) DEFAULT NULL,
  `returnAddr` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `createName` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `updateName` varchar(255) DEFAULT NULL,
  `isDelete` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`regID`),
  UNIQUE KEY `regID_UNIQUE` (`regID`),
  KEY `pgrID_idx` (`regpgrID`),
  KEY `schclassID_idx` (`schclassID`),
  CONSTRAINT `regpgrID` FOREIGN KEY (`regpgrID`) REFERENCES `passanger` (`pgrID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `schclassID` FOREIGN KEY (`schclassID`) REFERENCES `class` (`claID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration`
--

LOCK TABLES `registration` WRITE;
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
INSERT INTO `registration` VALUES (1,2017,3,9,'AM',NULL,NULL,NULL,NULL,'2 WAY RETURN',NULL,'12 MONTHS',NULL,NULL,'555 PICKUP STREET',NULL,NULL,'555 RETURN STREET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-26 12:54:50
