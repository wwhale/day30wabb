module.exports = function(connection, Sequelize){
    
        var Registration = connection.define('registration', {
            regID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            regpgrID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'passanger',
                    key: 'pgrID'
                }
            },
            schclassID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'class',
                    key: 'claID'
                }
            },

            year: {
                type: Sequelize.INTEGER(4),
                allowNull: false
            },
            sess: {
                type: Sequelize.STRING,
                allowNull: false
            },
            fees: {
                type: Sequelize.DECIMAL,
                allowNull: false
            },
            payScheme: {
                type: Sequelize.STRING,
                allowNull: false
            },
            serviceType: {
                type: Sequelize.STRING,
                allowNull: false
            },
            pickupAddr:{
                type: Sequelize.STRING,
                allowNull: false
            },
            returnAddr:{
                type: Sequelize.STRING,
                allowNull: true
            }
        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Registration;
    }