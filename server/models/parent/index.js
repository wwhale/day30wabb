module.exports = function(connection, Sequelize){
    
        var Parent = connection.define('parent', {
            parID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            pgrID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'passanger',
                    key: 'pgrID'
                }
            },


            parName: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            relation: {
                type: Sequelize.STRING,
                allowNull: false
            },

        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Parent;
    }

